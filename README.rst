tuned
=====

Tuned is a profile-based system tuning tool.
tuned-profiles-realtime is required by rt-setup (for RT kernel).

To build a new version of the RPM:

  - update the TAG variable in the build.sh script
  - push your changes to check that the RPM can be built by gitlab-ci
  - tag the repository and push the tag for the RPM to be uploaded to artifactory rpm-ics repo
