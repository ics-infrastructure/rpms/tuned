#!/bin/bash

# Define the tag to use
TAG="imports/c7/tuned-2.11.0-5.el7_7.1"

git clone https://git.centos.org/git/centos-git-common.git
git clone https://git.centos.org/git/rpms/tuned

cd tuned
git checkout ${TAG} && ../centos-git-common/get_sources.sh -b c7
rpmbuild --define "%_topdir `pwd`" --target noarch -ba SPECS/tuned.spec
tree RPMS
